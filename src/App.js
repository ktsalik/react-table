import React from 'react';
import './App.css';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: [],
      users: [],
      page: 1,
      pageCount: 1,
      itemsPerPage: 10,
    };

    var vm = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole');
    xhr.send();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        var data = JSON.parse(xhr.responseText);
        vm.setState({
          data: data,
          users: data.slice(0, vm.state.itemsPerPage),
          pageCount: Math.ceil(data.length / vm.state.itemsPerPage),
          loading: false,
        });
      }
    };
  }

  previousPage() {
    var page = this.state.page;
    var ipp = this.state.itemsPerPage;
    if (--page >= 1) {
      this.setState({
        page,
        users: this.state.data.slice((page - 1) * ipp, page * ipp),
      });
    } else {
      page = 1;
    }
  }

  nextPage() {
    var page = this.state.page;
    var ipp = this.state.itemsPerPage;
    var maxPage = Math.ceil(this.state.data.length / ipp);
    if (++page <= maxPage) {
      this.setState({
        page,
        users: this.state.data.slice((page - 1) * ipp, page * ipp),
      });
    } else {
      page = maxPage;
    }
  }

  changePage(page) {
    var ipp = this.state.itemsPerPage;
    this.setState({
      page,
      users: this.state.data.slice((page - 1) * ipp, page * ipp),
    });
  }

  changeItemsPerPage(e) {
    var ipp = parseInt(e.target.value);
    if (ipp) {
      this.setState({
        itemsPerPage: ipp,
        users: this.state.data.slice(0, ipp, this.state.page * ipp),
        pageCount: Math.ceil(this.state.data.length / ipp),
        page: 1,
      });
    } else {
      this.setState({
        itemsPerPage: 1,
      });
    }
  }

  render() {
    return (
      <div className="app">
        <table className="table is-striped is-hoverable">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {this.state.loading && (<tr><td>Loading Data</td></tr>)}
            {this.state.users.map((user, i) => {
              return (
                <tr key={i}>
                  <td>{user.first} {user.last}</td>
                  <td>{user.email}</td>
                  <td>{user.address}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        
        <div className="controls">
          <button className="button" onClick={this.previousPage.bind(this)}>Previous</button>
          {[...Array(this.state.pageCount)].map((index, i) => {
            return (
              <button className={`button ${(i + 1) === this.state.page ? 'is-info' : ''}`} key={i} onClick={this.changePage.bind(this, i + 1)}>
                {i + 1}
              </button>
            );
          })}
          <button className="button" onClick={this.nextPage.bind(this)}>Next</button>
          <br />
          Items per page
          <br />
          <input type="text" className="input" onChange={this.changeItemsPerPage.bind(this)} value={this.state.itemsPerPage} />
        </div>
      </div>
    );
  }
}

export default App;
